from definitions import *


class Polynom(object):
    def __init__(self, func: callable, derivative: callable, a: float, b: float, precision=EPSILON):
        self.limits = (a, b)
        self.func = func
        self.derivative = derivative
        self._precision = precision

    @property
    def limits(self):
        return self._a, self._b

    @limits.setter
    def limits(self, limits):
        self._a, self._b = limits

    def bisection(self):
        a, b = self.limits
        x_i = 0
        iter_counter = 0
        if not all([self.func(a), self.func(b)]):
            return 0
        while(b-a)/2 > self._precision:
            dx = (b - a) / 2
            x_i = a + dx
            if not self.func(a)*self.func(x_i) > 0:
                b = x_i
            else:
                a = x_i
            iter_counter += 1
        print(f"Bisection. Iterations: {iter_counter}, result: {x_i}")
        return x_i

    def secant(self):
        a, b = self.limits
        iter_counter = 0
        while abs(b-a) > self._precision:
            a = b - (b - a) * self.func(b) / (self.func(b) - self.func(a))
            b = a + (a - b) * self.func(a) / (self.func(a) - self.func(b))
            iter_counter += 1
        print(f"Secant. Iterations: {iter_counter}, result: {b}")
        return b

    def newton(self):
        a, b = self.limits
        iter_counter = 0
        b = a - self.func(a)/self.derivative(a)
        while(abs(b - a) > self._precision):
            a = b
            b = b - self.func(b)/self.derivative(b)
            iter_counter += 1
        print(f"Netwon\'s. Iterations: {iter_counter}, result: {b}")
        return b
