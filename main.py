from comp_methods import Polynom


def custom_function(x):
    """A function from my task"""
    return -(x**4) + 3*x**3 - 2*x + 2


def custom_derivative(x):
    """The first-order derivative for the function above"""
    return -4*x**3 + 9*x*x - 2


if __name__ == '__main__':
    func = custom_function
    dfdx = custom_derivative
    a, b = -1.5, 2.5
    p = Polynom(func, dfdx, a, b)
    p.bisection()
    p.secant()
    p.newton()
